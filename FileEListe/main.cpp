#include <iostream>
#include <fstream>
#include <string.h>
#include <cstdlib>
#include <vector>
#include "prova.h"

using namespace std;


int main() {
	prova test;
	vector<persona> dipendente;
	persona per;
	int x;

	do {
		cout << "\n1 per aggiungere dipendenti\n2 per visualizzare\n3 per leggere dal file\n4 per salvare\n0 per uscire\nscegli>";
		cin >> x;

		switch (x) {
		case 1:
			system("cls");
			test.agg(&dipendente, per);
			break;
		case 2:
			system("cls");
			test.stampa(dipendente);
			break;
		case 3:
			system("cls");
			test.stampaFile();
			break;
		case 4:
			system("cls");
			test.salvaFile(dipendente);
			break;
		}
	} while (x != 0);

	return 0;
}
