#include "prova.h"

void prova::agg(vector<persona>* per, persona x) {
	string n, c;
	int e, i;

	do {
		cout << endl << "NOME : ";
		cin >> n;
		x.nome = n;
		cout << "COGNOME : ";
		cin >> c;
		x.cognome = c;
		cout << "ETA : ";
		cin >> e;
		x.eta = e;

		per->push_back(x);

		cout << "\n1 per Aggiungere un altro Dipendente\n0 per Terminare\nscegli >";
		cin >> i;
	} while (i != 0);
}

void prova::stampa(vector<persona> num) {
	system("cls");
	for (auto i = num.begin(); i != num.end(); ++i) {
		cout << endl << i->nome << endl;
		cout << i->cognome << endl;
		cout << i->eta << endl;
	}
}

void prova::salvaFile(vector<persona> per) {
	ofstream fp("demo.txt", ios::app);

	if (fp.is_open()) {
		for (auto i = per.begin(); i != per.end(); ++i) {
			fp << i->nome << " " << i->cognome << " " << i->eta << "\n";
		}
		fp.close();
	}
	cout << "\ni dati sono salvati\n\n";

}

void prova::stampaFile() {

	ifstream ReadFile;
	ReadFile.open("demo.txt");
	string line;
	int x;

	char nome[20];
	char cognome[20];
	int eta;

	if (ReadFile.is_open()) {
		while (!ReadFile.eof()) {
			ReadFile >> nome;
			ReadFile >> cognome;
			ReadFile >> eta;

			if (!ReadFile.eof())
				cout << nome << " " << cognome << " " << eta << endl;
		}
	}
	else {
		cout << "Unable to open file";
	}
}